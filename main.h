#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>

#define GLUT_DISABLE_ATEXIT_HACK
#define PI 3.1415926535898

//draw func
void draw();

//update fps
void update_time();

//draw a circle
void drawCircle();

//Initialisation func
void init();

//Keyboard event func
void keyboard(unsigned char key, int x, int y);

//Key up event func
void OnKeyUp(unsigned char Key, int X, int Y);

//Mouse button event func
void mouse(int button, int state, int x, int y);

//Mouse move event func
void mouseMove(int x, int y);

//NoEvent event func
void idle();

//TouchWindow event func
void reshape(int width, int height);


